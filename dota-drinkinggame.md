#### Take a shot if:
- You die whilst carrying a rapier
- You end the game without any kills
- You die to neutrals
- Whenever you get stuck after using a mobility item or spell (e.g. Blink, Forcestaff or Puck orb) you have to take a shot.
- You die with the gem
- You lose your hero / Ask am i death.

#### Everyone takes a shot if:
- We get teamwiped
- The enemy kills Roshan

#### Everyone else takes a shot if:
- You get a rampage
- Someone buys a rapier

### Random rules (1 per game)

Get a random number between the min and max rule and then play the game with that rule.

###### (1) Protect the president:
Assign 1 player, you have to protect him/her at all cost. All rules are void for the president 
and he doesn't have to drink. If the president dies, everyone else takes a shot. 

The president can't suicide themselves (e.g. Pudge).

###### (2) Balance in all things:
At the end of the game take the average of all the kills, the players more then 3 kills away from this average
have to take a shot. 

###### (3) Codenames:
Everyone gives themselves a codename, if you call someone by their real name, you take a shot.

###### (4) The hare:
Randomly pick one of you. This person's objective is to finish the next game not having the least amount of kills. 
If that person is last, he takes a shot, otherwise the others take a shot. 

The objective is to kill steal as much as possible.

###### (5) Pssst:
Each person mutes 2 other players (chosen randomly).
Everyone can basically here 2 other players at all time.

You have to work together to pass information around.

###### (6) Green peace:
Everytime you get the last hit on a tower you have to plant a tree on it's rubble.
If you forget/are unable to do so you have to take a shot. 

###### (7) The name of the game:
Everyone has to play a hero starting with the first letter from their name (Bob => Bristleback for example).

###### (8) RAINBOWWWWWWWWWWWS:
At the start pick a random color, you're only allowed to pick heroes with that color.

###### (9) Digging for gold:
If we miss all of the bounty runes at the 5min intervals at least once, we have to take a shot

###### (10) It takes 5 to tango:
End the game with at least 1 tango in your inventory, if you dont have a tango you have to take a shot.